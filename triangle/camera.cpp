#include "Camera.h"
#include <gtx/rotate_vector.hpp>

Camera::Camera(float fov, int w, int h, float n, float f)
{
	fovy = fov;
	width = w;
	height = h;
	near_ = n;
	far_ = f;
}

void Camera::move(const vec3 &vec){	
	position += normalize(vec);
}

void Camera::rt(float angle, const vec3 & vec)
{
	up = rotate(up, angle, vec);
	direction = rotate(direction, angle, vec);
}

void Camera::zoom(float value){
	fovy += value;
}

void Camera::look_at(const vec3 &pos, const vec3 &tar, const vec3 &u){
	position = pos;
	target = tar;
	up = normalize(u);
	direction = target - position;
}

void onKeyPress(char key){

}

vec3 Camera::get_right(){
	return cross(direction, up);
}

vec3 Camera::get_up(){
	return up;
}

vec3 Camera::get_dir(){
	return direction;
}

vec3 Camera::get_tar(){
	return target;
}


mat4 Camera::get_mvp()
{
	mat4 projection = perspective(glm::radians(45.0f), float(width / height), near_, far_);
	
	return projection * lookAt(position, position + direction, up);
}
