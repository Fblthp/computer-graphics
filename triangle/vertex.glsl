#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 TexCoord;
layout (location = 2) in vec3 Normal;

layout (location = 3) in vec3 Tang;

out vec2 TexC;                                                              
out vec3 WorldNormal; 
out vec3 WorldPos;
out vec3 Tangent;
uniform mat4 MVP;

void main() {
  gl_Position = MVP * vec4(Position, 1.0);
  TexC= TexCoord;
  WorldNormal = Normal;
  WorldPos = gl_Position.xyz;
  Tangent = Tang;
}

