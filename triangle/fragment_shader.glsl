 #version 330 core

in vec3 origColor;
out vec3 color;

 void main(){
   color = origColor;
 }