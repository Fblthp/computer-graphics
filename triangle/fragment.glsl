#version 330

in vec2 TexC;                                                   
in vec3 WorldNormal;           
in vec3 WorldPos;  
in vec3 Tangent;
out vec4 FragColor;

struct Lights
{
    vec3 Color;
    float AmbientIntensity;
    float DiffuseIntensity;
};

struct DirLight
{
    Lights Base; 
	vec3 Direction;
};

struct Attenuation                                                               
{                                                                                
    float Constant;                                                              
    float Linear;                                                                
    float Exp;                                                                   
}; 

struct PointLight                                                                     
{                                                                                      
    Lights Base;                                                               
    vec3 Position;                                                                       
    Attenuation Atten;                                                                
};   
  
uniform PointLight gPointLights; 
uniform DirLight gLight;
uniform sampler2D gSampler;
uniform sampler2D gNormalMap;
uniform sampler2D gSampler_1;
uniform int amount;


vec4 CalcLightInternal(vec3 Normal)
{
    vec4 AmbientColor = vec4(gLight.Base.Color, 1.0) *
                gLight.Base.AmbientIntensity;


    float DiffuseFactor = max(dot(gLight.Direction, normalize(Normal)), 0.0);

    vec4 DiffuseColor = vec4(gLight.Base.Color, 1.0f) * gLight.Base.DiffuseIntensity * 
           DiffuseFactor;

    return (AmbientColor + DiffuseColor);
}

vec4 CalcPointLight(vec3 Normal)
{
    vec3 LightDirection = WorldPos - gPointLights.Position;
    float Distance = length(LightDirection);
    LightDirection = normalize(LightDirection);
 
    vec4 Color = vec4(1.0, 1.0, 1.0, 1.0);
    float Attenuation =  (gPointLights.Atten.Constant +
                 gPointLights.Atten.Linear * Distance +
                 gPointLights.Atten.Exp * Distance * Distance) * 0.001;
 
    return (Color / Attenuation);
}

vec3 CalcBumpedNormal()
{
    vec3 Normal = normalize(WorldNormal);
    vec3 Tangent = normalize(Tangent);
    Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal);
    vec3 Bitangent = cross(Tangent, Normal);
    vec3 BumpMapNormal = (texture2D(gNormalMap, TexC.st)).xyz;
    BumpMapNormal = 2.0 * BumpMapNormal - vec3(1.0, 1.0, 1.0);
    vec3 NewNormal;
    mat3 TBN = mat3(Tangent, Bitangent, Normal);
    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    return NewNormal;
}

vec4 getColor(){
	int w = int(TexC.x * amount);
	int h = int(TexC.y * amount);

	if ((w + h) % 2 == 0){
		return texture2D(gSampler_1, TexC.st);
	}
	else{
		return texture2D(gSampler, TexC.st);
	}

}

void main()
{
	 vec3 NewNormal = CalcBumpedNormal();

     vec4 TotalLight = CalcLightInternal(NewNormal);
 
     TotalLight += CalcPointLight(NewNormal);

	 vec4 c = getColor();
     FragColor = c * (TotalLight);

}
