#version 330 core

layout (location = 0) in vec3 vertexColor;
layout (location = 1) in vec3 position;

out vec3 origColor;

uniform mat4 MVP;

void main(){
	gl_Position = MVP * vec4(position, 1.0);
	origColor = vertexColor;
}