#pragma once
#include <glm.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>

using namespace glm;

GLuint programId;

struct Lights
{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;

	Lights()
	{
		Color = vec3(1.0f, 1.0f, 1.0f);
		AmbientIntensity = 1.0f;
		DiffuseIntensity = 1.0f;
	}
};

struct DirLight : public Lights
{
	vec3 Direction;

	DirLight()
	{
		Direction = vec3(0.0f, 0.0f, 0.0f);
	}
};

struct PointLight : public Lights
{
	vec3 Position;

	struct
	{
		float Constant;
		float Linear;
		float Exp;
	} Attenuation;

	PointLight()

	{
		AmbientIntensity = 1.0f;
		Position = vec3(0.0f, 0.0f, 0.0f);
		Attenuation.Constant = 1.0f;
		Attenuation.Linear = 0.0f;
		Attenuation.Exp = 0.0f;
	}
};

struct {
	GLuint Color;
	GLuint AmbientIntensity;
	GLuint DiffuseIntensity;
	GLuint Position;

	struct{
		GLuint Constant;
		GLuint Linear;
		GLuint Exp;
	} Atten;
} m_pointLightsLocation;

struct {
	GLuint Color;
	GLuint AmbientIntensity;
	GLuint DiffuseIntensity;
	GLuint Direction;

} m_directionalLightsLocation;


DirLight DirectionalLight;
PointLight pLights;



void InitPointLights();
void SetPointLights();
void InitDirectionalLight();
void SetDirectionalLight();
void CreatePointLight();


void InitPointLights()
{
	m_pointLightsLocation.Color = glGetUniformLocation(programId, "gPointLights.Base.Color");
	m_pointLightsLocation.AmbientIntensity = glGetUniformLocation(programId, "gPointLights.Base.AmbientIntensity");
	m_pointLightsLocation.DiffuseIntensity = glGetUniformLocation(programId, "gPointLights.Base.DiffuseIntensity");
	m_pointLightsLocation.Position = glGetUniformLocation(programId, "gPointLights.Position");
	m_pointLightsLocation.Atten.Constant = glGetUniformLocation(programId, "gPointLights.Atten.Constant");
	m_pointLightsLocation.Atten.Linear = glGetUniformLocation(programId, "gPointLights.Atten.Linear");
	m_pointLightsLocation.Atten.Exp = glGetUniformLocation(programId, "gPointLights.Atten.Exp");
};

void SetPointLights()
{
	glUniform3f(m_pointLightsLocation.Color, pLights.Color.x, pLights.Color.y, pLights.Color.z);
	glUniform1f(m_pointLightsLocation.AmbientIntensity, pLights.AmbientIntensity);
	glUniform1f(m_pointLightsLocation.DiffuseIntensity, pLights.DiffuseIntensity);
	glUniform3f(m_pointLightsLocation.Position, pLights.Position.x, pLights.Position.y, pLights.Position.z);
	glUniform1f(m_pointLightsLocation.Atten.Constant, pLights.Attenuation.Constant);
	glUniform1f(m_pointLightsLocation.Atten.Linear, pLights.Attenuation.Linear);
	glUniform1f(m_pointLightsLocation.Atten.Exp, pLights.Attenuation.Exp);

}

void InitDirectionalLight(){
	m_directionalLightsLocation.Color = glGetUniformLocation(programId, "gLight.Base.Color");
	m_directionalLightsLocation.AmbientIntensity = glGetUniformLocation(programId, "gLight.Base.AmbientIntensity");
	m_directionalLightsLocation.Direction = glGetUniformLocation(programId, "gLight.Direction");
	m_directionalLightsLocation.DiffuseIntensity = glGetUniformLocation(programId, "gLight.Base.DiffuseIntensity");
}

void SetDirectionalLight(){
	vec3 lightColor = normalize(DirectionalLight.Color);
	vec3 direction = normalize(DirectionalLight.Direction);

	glUniform3f(m_directionalLightsLocation.Color, lightColor.x, lightColor.y, lightColor.z);
	glUniform1f(m_directionalLightsLocation.AmbientIntensity, DirectionalLight.AmbientIntensity);
	glUniform3f(m_directionalLightsLocation.Direction, direction.x, direction.y, direction.z);
	glUniform1f(m_directionalLightsLocation.DiffuseIntensity, DirectionalLight.DiffuseIntensity);
}

void CreatePointLight(){
	pLights.Color = vec3(1.0f, 1.0f, 1.0f);
	pLights.Position = vec3(15.0f, 20.0f, 5.0f);
	pLights.Attenuation.Constant = 1.0f;
	pLights.Attenuation.Linear = 1.0f;
	pLights.Attenuation.Exp = 1.0f;
}