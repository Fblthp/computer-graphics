#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <SOIL.h>
#include <glm.hpp>
#include <vector>
#include <sstream>
#include "Camera.h"
#include <cmath>
#include <string>
#include "Lights.h"

using namespace std;

//vertex buffers
GLuint arrayObject;
GLuint GridBufferObject;
GLuint CubeBufferObject;
GLuint IBO;

//textures
const int TEXTURE_AMOUNT = 4;
GLuint textureIds[TEXTURE_AMOUNT];
char* textureNames[] = { "texture_1.png", "texture_2.png", "texture_3.png", "norm.png"};


//vertices
struct Vertex
{
	vec3 coordinatePosition;
	vec2 texturePosition;
	vec3 normal;
	vec3 tangent;

	Vertex(){}

	Vertex(vec3 pos, vec2 tex)
	{
		coordinatePosition = pos;
		texturePosition = tex;
		normal = vec3(0.0f, 0.0f, 0.0f);
		tangent = vec3(0.0f, 0.0f, 0.0f);
	}
};


const int windowWidth = 1000;
const int windowHeight = 800;
const int lineAmount = 100;
int squareAmount = 10;
Camera camera(M_PI_4, windowWidth, windowHeight, 0.1f, 500.0f);

GLuint createShaderfromString(string shaderText, GLenum shaderType){
	GLuint shaderId = glCreateShader(shaderType);
	GLint length = shaderText.size();

	const char* shaderTextCstr = shaderText.c_str();
	glShaderSource(shaderId, sizeof(char), &shaderTextCstr, &length);
	glCompileShader(shaderId);

	GLint status;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
	if (!status) {
		GLchar log[1024];
		GLint logLength = sizeof(log);
		glGetShaderInfoLog(shaderId, logLength, &logLength, log);
		cout << log << endl;
		throw;
	}
	return shaderId;
}

string readShaderFromFile(string filename){
	ifstream input(filename);
	stringstream stringResult;
	string line;
	while (getline(input, line)){
		stringResult << line << endl;
	}
	return stringResult.str();
}

void CalculateNormals(unsigned int* pIndices, Vertex* pVertices, int VertexCount, int IndexCount)
{
	for (int i = 0; i < VertexCount; i += 3){

		vec3 v1 = pVertices[pIndices[i + 1]].coordinatePosition - pVertices[pIndices[i]].coordinatePosition;
		vec3 v2 = pVertices[pIndices[i + 2]].coordinatePosition - pVertices[pIndices[i]].coordinatePosition;

		vec3 Normal = cross(v1, v2); 
		Normal = normalize(Normal);

		float DeltaU1 = pVertices[pIndices[i + 1]].texturePosition.x - pVertices[pIndices[i]].texturePosition.x;
		float DeltaV1 = pVertices[pIndices[i + 1]].texturePosition.y - pVertices[pIndices[i]].texturePosition.y;
		float DeltaU2 = pVertices[pIndices[i + 2]].texturePosition.x - pVertices[pIndices[i]].texturePosition.x;
		float DeltaV2 = pVertices[pIndices[i + 2]].texturePosition.y - pVertices[pIndices[i]].texturePosition.y;

		float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);

		vec3 Tang;

		Tang.x = f * (DeltaV2 * v1.x - DeltaV1 * v2.x);
		Tang.y = f * (DeltaV2 * v1.y - DeltaV1 * v2.y);
		Tang.z = f * (DeltaV2 * v1.z - DeltaV1 * v2.z);

		Tang = normalize(Tang);

		pVertices[pIndices[i]].tangent += Tang;
		pVertices[pIndices[i + 1]].tangent += Tang;
		pVertices[pIndices[i + 1]].tangent += Tang;

		pVertices[pIndices[i]].normal += Normal;
		pVertices[pIndices[i + 1]].normal += Normal;
		pVertices[pIndices[i + 2]].normal += Normal;
	}
	for (int i = 0; i < VertexCount; i++){
		pVertices[i].tangent = normalize(pVertices[i].tangent);
	}
}

void SetGrid(int n, Vertex* result)
{
	for (int i = 0; i < 4 * n + 4; i += 4){
		for (int j = 0; j < 4; j++){
			result[i + j].coordinatePosition = vec3((i / 2) * (j / 2) + n * (j == 1) * 2,
				(i / 2) * (1 - j / 2) + n * (j == 3) * 2, 0);
			result[i + j].normal = vec3(0.0f, 1.0f, 0.0f);

		}
	}
}
/*
void SetGrid(int n, Vertex* result)
{
	for (int i = 0; i < n; i += 1){
		for (int j = 0; j < n; j += 1){
			result[i * n + j].coordinatePosition = vec3(i, j, 0);
			result[i + j].normal = vec3(0.0f, 1.0f, 0.0f);

		}
	}
}
*/
void initVertexObjects(){
	
	Vertex Vertices[404];
	SetGrid(lineAmount, Vertices);

	Vertex CubeVertices[] = {
		Vertex(vec3(16.0f, 19.0f, 0.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(16.0f, 19.0f, 2.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 2.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 0.0f), vec2(1.0f, 0.0f)),

		Vertex(vec3(14.0f, 21.0f, 0.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(14.0f, 21.0f, 2.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 2.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 0.0f), vec2(1.0f, 0.0f)),

		Vertex(vec3(14.0f, 19.0f, 2.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(14.0f, 21.0f, 2.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 2.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(16.0f, 19.0f, 2.0f), vec2(1.0f, 0.0f)),

		Vertex(vec3(14.0f, 19.0f, 0.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(14.0f, 19.0f, 2.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(14.0f, 21.0f, 2.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(14.0f, 21.0f, 0.0f), vec2(1.0f, 0.0f)),

		Vertex(vec3(14.0f, 19.0f, 0.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(14.0f, 19.0f, 2.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(16.0f, 19.0f, 2.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(16.0f, 19.0f, 0.0f), vec2(1.0f, 0.0f)),

		Vertex(vec3(14.0f, 19.0f, 0.0f), vec2(0.0f, 0.0f)),
		Vertex(vec3(14.0f, 21.0f, 0.0f), vec2(0.0f, 1.0f)),
		Vertex(vec3(16.0f, 21.0f, 0.0f), vec2(1.0f, 1.0f)),
		Vertex(vec3(16.0f, 19.0f, 0.0f), vec2(1.0f, 0.0f))
	};
	unsigned int Indices[] = {0, 1, 2, 2, 3, 0,
							4, 5, 6, 6, 7, 4,
							8, 9, 10, 10, 11, 8, 
							12, 13, 14, 14, 15, 12, 
							16, 17, 18, 18, 19, 16, 
							20, 21, 22, 22, 23, 20};

	CalculateNormals(Indices, CubeVertices, 36, 24);

	glGenVertexArrays(1, &arrayObject);
	glBindVertexArray(arrayObject);

	glGenBuffers(1, &GridBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, GridBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &CubeBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, CubeBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVertices), CubeVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

}

GLuint makeShaderProgram(){
	GLuint program = glCreateProgram();

	glAttachShader(program, createShaderfromString(readShaderFromFile("vertex.glsl"), GL_VERTEX_SHADER));
	glAttachShader(program, createShaderfromString(readShaderFromFile("fragment.glsl"), GL_FRAGMENT_SHADER));
	glLinkProgram(program);

	GLint status = GL_UNSIGNED_INT;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status){
		return GL_UNSIGNED_INT;
	}
	return program;
}

void render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(programId);

	mat4 mvp = camera.get_mvp();

	DirectionalLight.Direction = camera.get_dir();
	//pLights.Position = camera.get_dir();

	GLuint MatrixId = glGetUniformLocation(programId, "MVP");
	glUniformMatrix4fv(MatrixId, 1, GL_FALSE, &mvp[0][0]);

	GLuint normalMapLocation = glGetUniformLocation(programId, "gNormalMap");
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, textureIds[3]);
	glUniform1i(normalMapLocation, 1);

	SetDirectionalLight();
	SetPointLights();

	GLuint Texture = glGetUniformLocation(programId, "gSampler");
	glUniform1i(Texture, 0);

	GLuint Texture_1 = glGetUniformLocation(programId, "gSampler_1");
	glUniform1i(Texture_1, 2);

	GLuint sq = glGetUniformLocation(programId, "amount");
	glUniform1i(sq, squareAmount);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER, CubeBufferObject);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureIds[0]);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, textureIds[2]);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)32);


	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

	//i know it looks the same, I'll do something wit that later
	glBindBuffer(GL_ARRAY_BUFFER, GridBufferObject);

	glBindTexture(GL_TEXTURE_2D, textureIds[1]);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)32);


	glDrawArrays(GL_LINES, 0, 404);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);

	glutSwapBuffers();
}

void keyPress(unsigned char key, int x, int y){
	if (key == 'w')
		camera.move(camera.get_dir());
	if (key == 's')
		camera.move(-camera.get_dir());
	if (key == 'd')
		camera.move(camera.get_right());
	if (key == 'a')
		camera.move(-camera.get_right());
	if (key == 'o')
		camera.move(camera.get_up());
	if (key == 'l')
		camera.move(-camera.get_up());

	vec3 tow = { 0.0f, 0.0f, 1.0f };
	if (key == 'r')
		camera.rt(0.05f, camera.get_right());
	if (key == 'q')
		camera.rt(0.05f, tow);
	if (key == 'f')
		camera.rt(-0.05f, camera.get_right());
	if (key == 'e')
		camera.rt(-0.05f, tow);

	if (key == 'n')
		DirectionalLight.AmbientIntensity += 0.1f;
	if (key == 'm')
		DirectionalLight.AmbientIntensity -= 0.1f;

	if (key == 'i')
		DirectionalLight.DiffuseIntensity -= 0.1f;
	if (key == 'k')
		DirectionalLight.DiffuseIntensity += 0.1f;


	if (key == 'c')
		DirectionalLight.Color.x += 0.1f;
	if (key == 'x')
		DirectionalLight.Color.y += 0.1f;
	if (key == 'z')
		DirectionalLight.Color.z += 0.1f;
	if (key == 'h')
		squareAmount += 1;
	if (key == 'y')
		squareAmount -= 1;
}

void LoadTextures(int i){
	int texh, texw;
	unsigned char* image =
		SOIL_load_image(textureNames[i], &texw, &texh, 0, SOIL_LOAD_RGB);
	glGenTextures(1, &textureIds[i]);
	glBindTexture(GL_TEXTURE_2D, textureIds[i]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texw, texh, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}


int main(int argc, char** argv){
	camera.look_at(vec3(15.0f, 0.0f, 15.0f),
				   vec3(15.0f, 20.0f, 0.0f),
				   vec3(0.0f, 1.0f, 0.0f));

	DirectionalLight.AmbientIntensity = 1.0f;
	DirectionalLight.Color = vec3(1.0f, 1.0f, 1.0f);
	DirectionalLight.DiffuseIntensity = 1.0f;

	CreatePointLight();

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutCreateWindow("Fblthp");
	glutDisplayFunc(render);
	glutIdleFunc(render);
	glutKeyboardFunc(keyPress);
	glewExperimental = GL_TRUE;

	auto res = glewInit();
	if (res != GLEW_OK){
		cout << glewGetErrorString(res) << endl;
		throw;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	for (int i = 0; i < TEXTURE_AMOUNT; i++){
		LoadTextures(i);
	}

	programId = makeShaderProgram();

	initVertexObjects();

	InitDirectionalLight();
	InitPointLights();

	glutMainLoop();
	return 0;
}
