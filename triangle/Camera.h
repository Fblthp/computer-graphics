#include <glm.hpp>
#include <gtc/matrix_transform.hpp> 

using namespace glm;

class Camera
{
	public:
		Camera(float fov, int w, int h, float n, float f);
		void move(const vec3 &vec);
		void rt(float angle, const vec3 &vec);
		void zoom(float value);
		void look_at(const vec3 &Pos, const vec3 &Tar, const vec3 &Up);
		void onKeyPress(char key);
		vec3 get_right();
		vec3 get_up();
		vec3 get_dir();
		vec3 get_tar();
		mat4 get_mvp();

		
	private:
		int width, height;
		float fovy, near_, far_, left, zm;
		vec3 position, up, direction, target;
};

